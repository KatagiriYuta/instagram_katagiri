class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  def index
    if logged_in?
      @post  = current_user.posts.build
      @caption_items = current_user.caption.paginate(page: params[:page]).search(params[:search])
    end
  end

end