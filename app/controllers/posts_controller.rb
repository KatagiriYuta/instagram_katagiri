class PostsController < ApplicationController


  
  # この行を追加する
  before_action :set_post, only: [:show, :destroy]

      def index
        @posts = Post.limit(10).includes(:photos, :user).order('created_at DESC')
        @posts = Post.search(params[:search]).order('created_at DESC')
      end

      def new
        @post = Post.new
      end
    
      def create
        @post = Post.new(post_params)
        if @post.save
          if params[:images]
            params[:images].each do |img|
              @post.photos.create(image: img)
            end
          end
    
          redirect_to posts_path
          flash[:notice] = "投稿が保存されました"
        else
          redirect_to posts_path
          flash[:alert] = "投稿に失敗しました"
        end
      end


  def show
    @photos = @post.photos
  end

  def destroy
    if @post.user == current_user
      return flash[:notice] = "投稿が削除されました" if @post.destroy
      flash[:alert] = "削除に失敗しました"
    end
    redirect_to root_path
  end

  private
    def post_params
      params.require(:post).permit(:caption).merge(user_id: current_user.id)
    end

    def set_post
      @post = Post.find_by(id: params[:id])
    end

end
